#ifndef MYHALOTHREAD_H
#define MYHALOTHREAD_H

#include <QThread>
#include <windows.h>

class myHaloThread : public QThread
{
    Q_OBJECT
public:
    myHaloThread( HANDLE );

protected:
    void run();

signals:
    void playerLeftVehicle();
    void playerEnteredVehicle();
    void playerZoomedIn();
    void playerZoomedOut();
    void playerRespawned();

public slots:

private:
    HANDLE hProcess;
    DWORD playerBase;
    void calculatePlayerBase();

};

#endif // MYHALOTHREAD_H
