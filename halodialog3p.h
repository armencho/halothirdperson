#ifndef HALODIALOG3P_H
#define HALODIALOG3P_H

#include <windows.h>
#include <QDialog>
#include <QSettings>
#include <QShortcut>
//#include <qsystemhotkey.h>

#include "myhalothread.h"
#include "systemhotkey.h"

namespace Ui {
class haloDialog3p;
}

class haloDialog3p : public QDialog
{
    Q_OBJECT

public:
    explicit haloDialog3p(QWidget *parent = 0);
    ~haloDialog3p();

public slots:
    void setSliderOneText(int);
    void setSliderTwoText(int);
    void setSliderThreeText(int);

    void setInOut(int);
    void setUpDown(int);
    void setLeftRight(int);

    void pushButtonToggled(bool);

    //void checkBox1Toggled(bool);
    //void checkBox2Toggled(bool);

    void playerZoomedIn();
    void playerZoomedOut();
    void playerEnteredVehicle();
    void playerLeftVehicle();
    void playerRespawned();

    void hotkeyPressed( int );
    void setInHotkey();

private:
    Ui::haloDialog3p *ui;
    HANDLE hProcess;
    DWORD playerBase;
    void enableHack();
    void disableHack();
    QSettings* settings;
    bool hackEnabled;
    myHaloThread* theHaloThread;
    SystemHotkey* hotkeys;

    int onOffHotkey;
    int inHotkey;
    int outHotkey;
    int upHotkey;
    int downHotkey;
    int leftHotkey;
    int rightHotkey;

    //QSystemHotkey *hotkeys;
    //QShortcut* onOffHotkey;
};

#endif // HALODIALOG3P_H
