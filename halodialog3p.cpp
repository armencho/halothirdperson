#include "halodialog3p.h"
#include "ui_halodialog3p.h"

#include <QString>
#include <iostream>

#define IS_DOWN(x) bool(x & 0x80)

void enableHack(HANDLE hProcess);
void disableHack(HANDLE hProcess);

haloDialog3p::haloDialog3p(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::haloDialog3p)
{
    ui->setupUi(this);
    QPixmap picture(":/images/halo.PNG");
    ui->label_4->setPixmap(picture);

    connect( ui->horizontalSlider, SIGNAL(valueChanged(int)),this, SLOT(setSliderOneText(int)));
    connect( ui->horizontalSlider_2, SIGNAL(valueChanged(int)),this, SLOT(setSliderTwoText(int)));
    connect( ui->horizontalSlider_3, SIGNAL(valueChanged(int)),this, SLOT(setSliderThreeText(int)));

    connect( ui->horizontalSlider, SIGNAL(valueChanged(int)),this, SLOT(setInOut(int)));
    connect( ui->horizontalSlider_2, SIGNAL(valueChanged(int)),this, SLOT(setUpDown(int)));
    connect( ui->horizontalSlider_3, SIGNAL(valueChanged(int)),this, SLOT(setLeftRight(int)));

    connect( ui->pushButton, SIGNAL(toggled(bool)),this, SLOT(pushButtonToggled(bool)));

    connect( ui->pushButton_2, SIGNAL(clicked()), this, SLOT(setInHotkey()));
    //connect( ui->pushButton_3, SIGNAL(clicked()), this, SLOT(setHotkey()));

    settings = new QSettings((QApplication::applicationDirPath() + "/thirdperson.ini"), QSettings::IniFormat);
    settings->beginGroup("Main");
    ui->horizontalSlider->setValue((settings->value("InOut").toInt()));
    ui->horizontalSlider_2->setValue((settings->value("UpDown").toInt()));
    ui->horizontalSlider_3->setValue((settings->value("LeftRight").toInt()));
    settings->endGroup();
    settings->beginGroup("Opts");
    ui->checkBox->setChecked((settings->value("fixZoom").toBool()));
    ui->checkBox_2->setChecked((settings->value("fixVehicle").toBool()));
    settings->endGroup();
    settings->beginGroup("Hotkeys");

    if ( !settings->contains("onOffHotkey") )
        onOffHotkey = VK_F10;
    else
        onOffHotkey = settings->value("onOffHotkey").toInt();

    if ( !settings->contains("inHotkey") )
        inHotkey = VK_NUMPAD1;
    else
        inHotkey = settings->value("inHotkey").toInt();

    if ( !settings->contains("outHotkey") )
        outHotkey = VK_NUMPAD3;
    else
        outHotkey = settings->value("outHotkey").toInt();

    if ( !settings->contains("upHotkey") )
        upHotkey = VK_NUMPAD8;
    else
        upHotkey = settings->value("upHotkey").toInt();

    if ( !settings->contains("downHotkey") )
        downHotkey = VK_NUMPAD2;
    else
        downHotkey = settings->value("downHotkey").toInt();

    if ( !settings->contains("leftHotkey") )
        leftHotkey = VK_NUMPAD4;
    else
        leftHotkey = settings->value("leftHotkey").toInt();

    if ( !settings->contains("rightHotkey") )
        rightHotkey = VK_NUMPAD6;
    else
        rightHotkey = settings->value("rightHotkey").toInt();

    //ui->pushButton_2->setText(((settings->value("InHotkey"))));
    //ui->pushButton_3->setText((settings->value("OutHotkey")));
    settings->endGroup();

    hotkeys = new SystemHotkey();
    hotkeys->addKey(onOffHotkey);
    hotkeys->addKey(inHotkey);
    hotkeys->addKey(outHotkey);
    hotkeys->addKey(upHotkey);
    hotkeys->addKey(downHotkey);
    hotkeys->addKey(leftHotkey);
    hotkeys->addKey(rightHotkey);
    connect( hotkeys, SIGNAL(hotkeyPressed(int)), this, SLOT(hotkeyPressed(int)) );
    hotkeys->listen();

    QString sliderText = "In and Out: ";
    QString textValue = QString::number(ui->horizontalSlider->value() / 10.0);
    sliderText.append(textValue);
    ui->label->setText(sliderText);

    sliderText = "Up and Down: ";
    textValue = QString::number(ui->horizontalSlider_2->value() / 10.0);
    sliderText.append(textValue);
    ui->label_2->setText(sliderText);

    sliderText = "Left and Right: ";
    textValue = QString::number(ui->horizontalSlider_3->value() / 10.0);
    sliderText.append(textValue);
    ui->label_3->setText(sliderText);


    HWND hWnd = FindWindow(0, L"Halo");
    if(hWnd == 0)
    {
        MessageBox(0, L"Error cannot find window.", L"Error", MB_OK|MB_ICONERROR);
        return;
    }
    else
    {
        DWORD process_ID;
        GetWindowThreadProcessId(hWnd, &process_ID);
        hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, process_ID);
        if(!hProcess)
        {
             MessageBox(0, L"Could not open the process!", L"Error!", MB_OK|MB_ICONERROR);
             return;
        }
    }

    ui->pushButton->toggle();
    if (hackEnabled)
    {
        theHaloThread = new myHaloThread(hProcess);
        connect( theHaloThread, SIGNAL(playerEnteredVehicle()), this, SLOT(playerEnteredVehicle()));
        connect( theHaloThread, SIGNAL(playerLeftVehicle()), this, SLOT(playerLeftVehicle()));
        connect( theHaloThread, SIGNAL(playerZoomedIn()), this, SLOT(playerZoomedIn()));
        connect( theHaloThread, SIGNAL(playerZoomedOut()), this, SLOT(playerZoomedOut()));
        connect( theHaloThread, SIGNAL(playerRespawned()), this, SLOT(playerRespawned()));
        connect( theHaloThread, &myHaloThread::finished, theHaloThread, &QObject::deleteLater);
        theHaloThread->start();
    }
}


void haloDialog3p::setSliderOneText( int value )
{
    QString sliderOneText = "In and Out: ";
    QString textValue = QString::number(value / 10.0);
    sliderOneText.append(textValue);
    ui->label->setText(sliderOneText);
}

void haloDialog3p::setSliderTwoText( int value )
{
    QString sliderTwoText = "Up and Down: ";
    QString textValue = QString::number(value / 10.0);
    sliderTwoText.append(textValue);
    ui->label_2->setText(sliderTwoText);
}

void haloDialog3p::setSliderThreeText( int value )
{
    QString sliderThreeText = "Left and Right: ";
    QString textValue = QString::number(value / 10.0);
    sliderThreeText.append(textValue);
    ui->label_3->setText(sliderThreeText);
}

void haloDialog3p::setInOut(int value)
{
    float myFloat = (value / 10.0);
    WriteProcessMemory(hProcess, (LPVOID)0x647654, &myFloat, sizeof(myFloat), NULL);
}

void haloDialog3p::setUpDown(int value)
{
    float myFloat = (value / 10.0);
    WriteProcessMemory(hProcess, (LPVOID)0x647650, &myFloat, sizeof(myFloat), NULL);
}

void haloDialog3p::setLeftRight(int value)
{
    float myFloat = (value / -10.0);
    WriteProcessMemory(hProcess, (LPVOID)0x64764C, &myFloat, sizeof(myFloat), NULL);
}

void haloDialog3p::pushButtonToggled( bool toggled )
{
    if (toggled)
    {
        enableHack();
        ui->pushButton->setText("ON");
    }
    else
    {
        disableHack();
        ui->pushButton->setText("OFF");
    }
}

void haloDialog3p::hotkeyPressed( int nVirtKey )
{
    if ( nVirtKey == onOffHotkey )
    {
        ui->pushButton->toggle();
        //std::cout << nVirtKey << "\n";
    }
    else if ( nVirtKey == inHotkey )
    {
        ui->horizontalSlider->setValue(ui->horizontalSlider->value() - 1 );
    }
    else if ( nVirtKey == outHotkey )
    {
        ui->horizontalSlider->setValue(ui->horizontalSlider->value() + 1 );
    }

    else if ( nVirtKey == upHotkey )
    {
        ui->horizontalSlider_2->setValue(ui->horizontalSlider_2->value() - 1 );
    }
    else if ( nVirtKey == downHotkey )
    {
        ui->horizontalSlider_2->setValue(ui->horizontalSlider_2->value() + 1 );
    }

    else if ( nVirtKey == leftHotkey )
    {
        ui->horizontalSlider_3->setValue(ui->horizontalSlider_3->value() - 1 );
    }
    else if ( nVirtKey == rightHotkey )
    {
        ui->horizontalSlider_3->setValue(ui->horizontalSlider_3->value() + 1 );
    }
}

void haloDialog3p::setInHotkey()
{
    BYTE keyState[256];
    char nVirtKey;

    while (true)
    {
        ui->pushButton_2->setText("?");
        qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
        GetKeyboardState( keyState );

        for ( int i = 0 ; i < 256 ; i++ )
        {
            if (IS_DOWN(keyState[i]))
            {
                nVirtKey = i;
                std::cout << nVirtKey << "\n";
                ui->pushButton_2->setText( QString(nVirtKey) );
                hotkeys->removeKey(inHotkey);
                inHotkey = i;
                hotkeys->addKey(inHotkey);
                //MapVirtualKey( nVirtKey , MAPVK_VK_TO_CHAR);
                return;
            }
        }
    }

}


haloDialog3p::~haloDialog3p()
{
    settings->beginGroup("Main");
    settings->setValue("InOut", ui->horizontalSlider->value() );
    settings->setValue("UpDown", ui->horizontalSlider_2->value() );
    settings->setValue("LeftRight", ui->horizontalSlider_3->value() );
    settings->endGroup();
    settings->beginGroup("Opts");
    settings->setValue("fixZoom", ui->checkBox->isChecked());
    settings->setValue("fixVehicle", ui->checkBox_2->isChecked());
    settings->endGroup();
    settings->beginGroup("Hotkeys");
    settings->setValue("onOffHotkey", onOffHotkey);
    settings->setValue("inHotkey", inHotkey);
    settings->setValue("outHotkey", outHotkey);
    settings->setValue("upHotkey", upHotkey);
    settings->setValue("downHotkey", downHotkey);
    settings->setValue("leftHotkey", leftHotkey);
    settings->setValue("rightHotkey", rightHotkey);
    settings->endGroup();
    settings->sync();
    delete ui;
}











































void haloDialog3p::playerZoomedIn()
{
    if ( ui->pushButton->text() == "ON" && ui->checkBox->isChecked() )
        disableHack();
}

void haloDialog3p::playerZoomedOut()
{
    if ( ui->pushButton->text() == "ON" && ui->checkBox->isChecked() )
         enableHack();
}


void haloDialog3p::playerEnteredVehicle()
{
    if ( ui->pushButton->text() == "ON" && ui->checkBox_2->isChecked() )
    {
        DWORD myData;
        DWORD oldProtect;
        PDWORD poldProtect = &oldProtect;

        VirtualProtectEx( hProcess, (LPVOID)(0x448C3C), 16, PAGE_EXECUTE_READWRITE, poldProtect );
        myData = 0x4110148B;
        WriteProcessMemory(hProcess, (LPVOID)(0x448C3C), &myData, sizeof(myData), NULL);
        myData = 0x66181489;
        WriteProcessMemory(hProcess, (LPVOID)(0x448C40), &myData, sizeof(myData), NULL);
        //UNDO JUMP TO INLINE

        WORD myWord = 0x7CD0;
        WriteProcessMemory(hProcess, (LPVOID)(0x00647498), &myWord, sizeof(myWord), NULL);

        myWord = 0x3F80;
        WriteProcessMemory(hProcess, (LPVOID)(0x006474B6), &myWord, sizeof(myWord), NULL);
    }

    /*if ( ui->pushButton->text() == "ON" && ui->checkBox_2->isChecked() )
        disableHack();*/
}

void haloDialog3p::playerLeftVehicle()
{
   if ( ui->pushButton->text() == "ON" )
        enableHack();
}


void haloDialog3p::playerRespawned()
{
    if ( ui->pushButton->text() == "ON" )
        enableHack();
}

void haloDialog3p::enableHack()
{   
    bool inVehicle;
    ReadProcessMemory( hProcess, (LPVOID)0x6474E4, &inVehicle, sizeof(inVehicle), NULL );

    if ( inVehicle && ui->checkBox_2->isChecked() )
    {
        return;
    }

    DWORD myData;
    DWORD oldProtect;
    PDWORD poldProtect = &oldProtect;

    VirtualProtectEx( hProcess, (LPVOID)0x400308, 44, PAGE_EXECUTE_READWRITE, poldProtect );

    myData = 0x4110148B;
    WriteProcessMemory(hProcess, (LPVOID)0x400308, &myData, sizeof(myData), NULL);
    myData = 0x7654FB81;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+4), &myData, sizeof(myData), NULL);
    myData = 0x1B740064;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+8), &myData, sizeof(myData), NULL);
    myData = 0x7648FB81;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+12), &myData, sizeof(myData), NULL);
    myData = 0x13740064;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+16), &myData, sizeof(myData), NULL);
    myData = 0x764CFB81;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+20), &myData, sizeof(myData), NULL);
    myData = 0x0B740064;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+24), &myData, sizeof(myData), NULL);
    myData = 0x7650FB81;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+28), &myData, sizeof(myData), NULL);
    myData = 0x03740064;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+32), &myData, sizeof(myData), NULL);
    myData = 0xE9181489;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+36), &myData, sizeof(myData), NULL);
    myData = 0x0004890F;
    WriteProcessMemory(hProcess, (LPVOID)(0x400308+40), &myData, sizeof(myData), NULL);
    //WRITE INLINE

    VirtualProtectEx( hProcess, (LPVOID)0x448C3C, 16, PAGE_EXECUTE_READWRITE, poldProtect );
    myData = 0xFB76C7E9;
    WriteProcessMemory(hProcess, (LPVOID)(0x448C3C), &myData, sizeof(myData), NULL);
    myData = 0x669090FF;
    WriteProcessMemory(hProcess, (LPVOID)(0x448C40), &myData, sizeof(myData), NULL);
    //WRITE JUMP INTO INLINE CODE

    VirtualProtectEx( hProcess, (LPVOID)0x448C1B, 2, PAGE_EXECUTE_READWRITE, poldProtect );
    WORD myWord = 0x9090;
    WriteProcessMemory(hProcess, (LPVOID)(0x448C1B), &myWord, sizeof(myWord), NULL);
    VirtualProtectEx( hProcess, (LPVOID)0x448C21, 2, PAGE_EXECUTE_READWRITE, poldProtect );
    myWord = 0x9090;
    WriteProcessMemory(hProcess, (LPVOID)(0x448C21), &myWord, sizeof(myWord), NULL);
    //NOP JUMP undoing 3rd person

    setInOut(ui->horizontalSlider->value());
    setUpDown(ui->horizontalSlider_2->value());
    setLeftRight(ui->horizontalSlider_3->value());

    float myFloat = 0.0;
    WriteProcessMemory(hProcess, (LPVOID)0x00647648, &myFloat, sizeof(myFloat), NULL);

    myWord = 0x7CD0;
    WriteProcessMemory(hProcess, (LPVOID)(0x00647498), &myWord, sizeof(myWord), NULL);

    myWord = 0x0000;
    WriteProcessMemory(hProcess, (LPVOID)(0x006474B6), &myWord, sizeof(myWord), NULL);

    hackEnabled = true;
}

void haloDialog3p::disableHack()
{
    DWORD myData;
    DWORD oldProtect;
    PDWORD poldProtect = &oldProtect;

    VirtualProtectEx( hProcess, (LPVOID)(0x448C3C), 16, PAGE_EXECUTE_READWRITE, poldProtect );
    myData = 0x4110148B;
    WriteProcessMemory(hProcess, (LPVOID)(0x448C3C), &myData, sizeof(myData), NULL);
    myData = 0x66181489;
    WriteProcessMemory(hProcess, (LPVOID)(0x448C40), &myData, sizeof(myData), NULL);
    //UNDO JUMP TO INLINE

    VirtualProtectEx( hProcess, (LPVOID)0x448C1B, 2, PAGE_EXECUTE_READWRITE, poldProtect );
    WORD myWord = 0x3474;
    WriteProcessMemory(hProcess, (LPVOID)(0x448C1B), &myWord, sizeof(myWord), NULL);
    VirtualProtectEx( hProcess, (LPVOID)0x448C21, 2, PAGE_EXECUTE_READWRITE, poldProtect );
    myWord = 0x2E74;
    WriteProcessMemory(hProcess, (LPVOID)(0x448C21), &myWord, sizeof(myWord), NULL);
    //UNDO JUMP undoing 3rd person

    bool inVehicle;
    ReadProcessMemory( hProcess, (LPVOID)0x6474E4, &inVehicle, sizeof(inVehicle), NULL );

    if (inVehicle)
    {
        WORD myWord = 0x7CD0;
        WriteProcessMemory(hProcess, (LPVOID)(0x00647498), &myWord, sizeof(myWord), NULL);

        myWord = 0x3F80;
        WriteProcessMemory(hProcess, (LPVOID)(0x006474B6), &myWord, sizeof(myWord), NULL);
    }
    else
    {
        WORD myWord = 0x76C0;
        WriteProcessMemory(hProcess, (LPVOID)(0x00647498), &myWord, sizeof(myWord), NULL);

        myWord = 0x0000;
        WriteProcessMemory(hProcess, (LPVOID)(0x006474B6), &myWord, sizeof(myWord), NULL);
    }

    hackEnabled = false;
}
