#ifndef QSYSTEMHOTKEY_H
#define QSYSTEMHOTKEY_H
#define MOD_NOREPEAT    0x4000
#define MOD_CONTROL     0x0002
#define MOD_ALT         0x0001

#include <vector>
#include <QThread>
#include <QWidget>
#include "windows/stdafx.h"

class QSystemHotkey : public QThread {
    Q_OBJECT

public:
    QSystemHotkey();

    void addKey(int keyID, UINT holdKey, UINT charKey);
    void removeKey(int keyID);
    int getHotkey(int position);

protected:
    void run();

public slots:
    void beginHotkeys(void);
    void haltHotkeys(void);

signals:
    void runHotkey(int position);

private:
    void run(int position);

    bool running;
    MSG msg;
    std::vector<int> keyIDs;
    std::vector<UINT> holdKeys;
    std::vector<UINT> charKeys;
};

#endif // QSYSTEMHOTKEY_H
