#include "qsystemhotkey.h"
#include <iostream>

QSystemHotkey::QSystemHotkey(){
    running = false;
}

void QSystemHotkey::addKey(int keyID, UINT holdKey, UINT charKey){
    keyIDs.push_back(keyID);
    holdKeys.push_back(holdKey);
    charKeys.push_back(charKey);
    //RegisterHotKey(NULL,keyID,holdKey | MOD_NOREPEAT,charKey);
}

void QSystemHotkey::removeKey(int keyID){
    for (int i = 0; i < (int)keyIDs.size(); ++i) {
        if(keyIDs.at(i) == keyID){
            UnregisterHotKey(NULL,keyIDs.at(i));
            keyIDs.erase(keyIDs.begin() + i);
        }
    }
}

int QSystemHotkey::getHotkey(int position){
    return keyIDs.at(position);
}

void QSystemHotkey::beginHotkeys(){
    running = true;
    start();
}

void QSystemHotkey::haltHotkeys(){
    running = false;
}

void QSystemHotkey::run(){
    for ( int i = 0 ; i < (int)keyIDs.size() ; i++ )
    {
        RegisterHotKey(NULL, keyIDs.at(i), holdKeys.at(i), charKeys.at(i));
        holdKeys.erase(holdKeys.begin() + i);
        charKeys.erase(charKeys.begin() + i);
    }
    while(running){
        GetMessage(&msg,NULL,0,0);
        TranslateMessage(&msg);
        DispatchMessage(&msg);
       /* if (msg.message == WM_HOTKEY)
        {
            std::cout << "WM_HOTKEY received\n";
        }*/
        if (msg.message == WM_HOTKEY){
            for (int i = 0; i < (int)keyIDs.size(); ++i) {
                if (msg.wParam == (unsigned int)keyIDs.at(i)) run(i);
            }
        }
    }
}

void QSystemHotkey::run(int position){
    emit runHotkey(position);
}
