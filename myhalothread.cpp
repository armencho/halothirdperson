#include "myhalothread.h"

#include <iostream>

myHaloThread::myHaloThread( HANDLE hProc )
{
    hProcess = hProc;
    playerBase = 0x0;
}

void myHaloThread::calculatePlayerBase()
{
    //      CALCULATE PlayerBase
    DWORD someOffset = 0;
    ReadProcessMemory( hProcess, (LPVOID)0x402AD4AC, &someOffset, sizeof(someOffset), 0 );
    someOffset &= 0xffff;
    someOffset += (someOffset * 2);
    ReadProcessMemory( hProcess, (LPVOID)someOffset, &someOffset, sizeof(someOffset), 0 );
    someOffset = (someOffset * 4) + 8;
    DWORD staticOffset = 0;
    ReadProcessMemory( hProcess, (LPVOID)0x007FB710, &staticOffset, sizeof(staticOffset), 0 );
    staticOffset += 0x34;
    ReadProcessMemory( hProcess, (LPVOID)staticOffset, &staticOffset, sizeof(staticOffset), 0 );
    staticOffset += someOffset;
    ReadProcessMemory( hProcess, (LPVOID)staticOffset, &staticOffset, sizeof(staticOffset), 0 );
    //std::cout << "PlayerBase: " <<  std::hex << "0x" << staticOffset << "\n";
    playerBase = staticOffset;
    // End of Calculation
}


void myHaloThread::run()
{
    bool inVehicle;
    bool vPrevious;
    BYTE isNotZoomed;
    BYTE zPrevious;
    DWORD playerBasePrevious;

    calculatePlayerBase();
    ReadProcessMemory( hProcess, (LPVOID)0x6474E4, &inVehicle, sizeof(inVehicle), NULL );
    ReadProcessMemory( hProcess, (LPVOID)(playerBase+0x320), &isNotZoomed, sizeof(isNotZoomed), NULL );

    while (true)
    {
        Sleep(1);
        vPrevious = inVehicle;
        zPrevious = isNotZoomed;
        playerBasePrevious = playerBase;

        calculatePlayerBase();
        ReadProcessMemory( hProcess, (LPVOID)0x6474E4, &inVehicle, sizeof(inVehicle), NULL );
        ReadProcessMemory( hProcess, (LPVOID)(playerBase+0x320), &isNotZoomed, sizeof(isNotZoomed), NULL );

        if ( !inVehicle && vPrevious == true )
        {
            emit playerLeftVehicle();
            std::cout << "EXIT VEHICLE!!!\n";
        }
        else if (inVehicle && vPrevious == false)
        {
            emit playerEnteredVehicle();
            std::cout << "ENTERED VEHICLE!!!\n";
        }
        else if ( ( isNotZoomed == 0x00 && zPrevious == 0xFF) || (isNotZoomed == 0x01 && zPrevious == 0x00) )
        {
            emit playerZoomedIn();
            std::cout << "ZOOMED IN!!!\n";
        }
        else if ( (isNotZoomed == 0xFF && zPrevious == 0x00) || (isNotZoomed == 0xFF && zPrevious == 0x01) )
        {
            emit playerZoomedOut();
            std::cout << "ZOOMED OUT!!!\n";
        }
        else if ( playerBase != playerBasePrevious && ( playerBasePrevious < 0x40000000 || playerBasePrevious > 0x50000000 ) )
        {
            Sleep(500);
            emit playerRespawned();
            std::cout << "PLAYER RESPAWNED!!!\n";
            std::cout << std::hex << playerBase << "\n";
        }

        //(respawned == 0x76C0 && rPrevious == 0x5CE0) || (respawned == 0x76C0 && rPrevious == 0x0000)
    }
    return;
}
