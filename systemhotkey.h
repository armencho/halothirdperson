#ifndef SYSTEMHOTKEY_H
#define SYSTEMHOTKEY_H

#include <QThread>
#include <QVector>
#include <windows.h>

class SystemHotkey : public QThread
{
    Q_OBJECT

public:
        SystemHotkey();
        void addKey( int );
        void removeKey( int );
        void listen();
protected:
        void run();

signals:
        void hotkeyPressed( int );

public slots:

private:
        QVector<int> hotkeys;
        bool running;


};

#endif // SYSTEMHOTKEY_H
