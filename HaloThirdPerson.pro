#-------------------------------------------------
#
# Project created by QtCreator 2014-08-04T18:39:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HaloThirdPerson
TEMPLATE = app


SOURCES += main.cpp\
        halodialog3p.cpp \
    myhalothread.cpp \
    systemhotkey.cpp

HEADERS  += halodialog3p.h \
    myhalothread.h \
    systemhotkey.h

FORMS    += halodialog3p.ui

RESOURCES += \
    resources.qrc
