#include "systemhotkey.h"

#define IS_DOWN(x) bool(x & 0x8000)
#define IS_TOGGLED(x) bool(x & 0x1)

SystemHotkey::SystemHotkey()
{
    running = false;
}

void SystemHotkey::addKey( int nVirtKey )
{
    hotkeys.push_back(nVirtKey);
}

void SystemHotkey::removeKey(int nVirtKey)
{
    for ( int i = 0 ; i < (int)hotkeys.size() ; i++ )
    {
        if(hotkeys.at(i) == nVirtKey)
        {
            hotkeys.erase(hotkeys.begin() + i);
        }
    }
}

void SystemHotkey::listen()
{
    running = true;
    start();
}

void SystemHotkey::run()
{
    int currentStateArray[256];
    int previousStateArray[256];
    for ( int i = 0 ; i < hotkeys.size() ; i++ )
    {
        currentStateArray[i] = GetAsyncKeyState( hotkeys.at(i) );
    }

    while (running)
    {
        Sleep(1);

        for ( int i = 0 ; i < hotkeys.size() ; i++ )
        {
            previousStateArray[i] = currentStateArray[i];
            currentStateArray[i] = GetAsyncKeyState( hotkeys.at(i) );
            if ( IS_DOWN(currentStateArray[i]) && !IS_DOWN(previousStateArray[i]) )
            {
                emit hotkeyPressed( hotkeys.at(i) );
            }
        }
    }
}
